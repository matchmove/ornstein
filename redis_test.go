package ornstein

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/garyburd/redigo/redis"
)

type TestObject struct {
	ID   string
	Name string
}

type TestObjects []TestObject

func TestRedisCache(t *testing.T) {
	bm, err := NewCache("redis", `{"conn": "127.0.0.1:6379"}`)
	if err != nil {
		t.Error("init err")
	}
	timeoutDuration := 10 * time.Second
	if err = bm.Set("ornstein", 1, timeoutDuration); err != nil {
		t.Error("set Error", err)
	}

	if !bm.IsExist("ornstein") {
		t.Error("check err")
	}

	time.Sleep(11 * time.Second)

	if bm.IsExist("ornstein") {
		t.Error("check err")
	}
	if err = bm.Set("ornstein", 1, timeoutDuration); err != nil {
		t.Error("set Error", err)
	}

	if v, _ := redis.Int(bm.Get("ornstein"), err); v != 1 {
		t.Error("get err")
	}

	if err = bm.Incr("ornstein"); err != nil {
		t.Error("Incr Error", err)
	}

	if v, _ := redis.Int(bm.Get("ornstein"), err); v != 2 {
		t.Error("get err")
	}

	if err = bm.Decr("ornstein"); err != nil {
		t.Error("Decr Error", err)
	}

	if v, _ := redis.Int(bm.Get("ornstein"), err); v != 1 {
		t.Error("get err")
	}
	bm.Flush("ornstein")
	if bm.IsExist("ornstein") {
		t.Error("delete err")
	}

	//test string
	if err = bm.Set("ornstein", "author", timeoutDuration); err != nil {
		t.Error("set Error", err)
	}
	if !bm.IsExist("ornstein") {
		t.Error("check err")
	}

	if v, _ := redis.String(bm.Get("ornstein"), err); v != "author" {
		t.Error("get err")
	}

	//test GetMulti
	if err = bm.Set("ornstein1", "author1", timeoutDuration); err != nil {
		t.Error("set Error", err)
	}
	if !bm.IsExist("ornstein1") {
		t.Error("check err")
	}

	vv := bm.GetMulti([]string{"ornstein", "ornstein1"})
	if len(vv) != 2 {
		t.Error("GetMulti ERROR")
	}
	if v, _ := redis.String(vv[0], nil); v != "author" {
		t.Error("GetMulti ERROR")
	}
	if v, _ := redis.String(vv[1], nil); v != "author1" {
		t.Error("GetMulti ERROR")
	}

	object := TestObject{
		ID:   "1",
		Name: "Lester",
	}

	seriealize, _ := json.Marshal(object)
	bm.Set("ornsteinObject", seriealize, timeoutDuration)
	if !bm.IsExist("ornsteinObject") {
		t.Error("check err")
	}

	var fetchedObject TestObject
	bytes, _ := redis.Bytes(bm.Get("ornsteinObject"), err)
	json.Unmarshal(bytes, &fetchedObject)

	if fetchedObject.ID != object.ID {
		t.Error("fetchObject ERROR")
	}

	if fetchedObject.Name != object.Name {
		t.Error("fetchObject ERROR")
	}

	// test clear all
	if err = bm.FlushAll(); err != nil {
		t.Error("clear all err")
	}
}
