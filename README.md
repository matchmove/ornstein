# ornstein
--
    import "bitbucket.org/matchmove/ornstein"


## Usage

#### type Cache interface
```go
type Cache interface {
	// get cached value by key.
	Get(key string) interface{}
	// GetMulti is a batch version of Get.
	GetMulti(keys []string) []interface{}
	// set cached value with key and expire time.
	Set(key string, val interface{}, timeout time.Duration) error
	// delete cached value by key.
	Flush(key string) error
	// increase cached int value by key, as a counter.
	Incr(key string) error
	// decrease cached int value by key, as a counter.
	Decr(key string) error
	// check if cached value exists or not.
	IsExist(key string) bool
	// clear all cache.
	FlushAll() error
	// start gc routine based on config string settings.
	StartAndGC(config string) error
}
```

Cache interface contains all behaviors for cache adapter.

#### type Instance func() Cache
```go
type Instance func() Cache
```
Instance is a function create a new Cache Instance

### func Register

```go
func Register(name string, adapter Instance)
```
Register makes a cache adapter available by the adapter name. If Register is called twice with the same name or if driver is nil, it panics.


### func NewCache

```go
func NewCache(adapterName, config string) (adapter Cache, err error)
```
NewCache Create a new cache driver by adapter name and config string. config need to be correct JSON as string: {"conn": "127.0.0.1:6379", "key":"cache-group-name"} it will start gc automatically.


### const DefaultKey

```go
const (
	// DefaultKey the default cache group for the cache
	DefaultKey = "OrnsteinRedisCache"
)
```

#### type RedisCache

```go
type RedisCache struct {
	p        *redis.Pool // redis connection pool
	conninfo string
	dbNum    int
	key      string
	password string
}
```
RedisCache is Redis cache adapter.

### func NewRedisCache

```go
func NewRedisCache() Cache
```
NewRedisCache creates a new instance of the redis cache

#### func (*RedisCache)  Get

```go
func (rc *RedisCache) Get(key string) interface{}
```
Get gets the cached value store in the given key


#### func (*RedisCache)  GetMulti

```go
func (rc *RedisCache) GetMulti(keys []string) []interface{}
```
Get get cache from redis.

#### func (*RedisCache)  Set

```go
func (rc *RedisCache) Set(key string, val interface{}, timeout time.Duration) error
```
Set stores the given val at the given key which expires in x seconds

#### func (*RedisCache)  Flush

```go
func (rc *RedisCache) Flush(key string) error
```

Flush deletes all the cache stored in the current cache group

#### func (*RedisCache)  IsExist

```go
func (rc *RedisCache) IsExist(key string) bool
```
IsExist checks if the key exist in the current cache group

#### func (*RedisCache)  Incr

```go
func (rc *RedisCache) Incr(key string) error
```

Incr increments the current key by 1

#### func (*RedisCache)  Incr

```go
func (rc *RedisCache) Decr(key string) error
```

Decr decrements the current key by 1

#### func (*RedisCache)  FlushAll

```go
func (rc *RedisCache) FlushAll() error
```
FlushAll deletes all cache in all groups

#### func (*RedisCache) StartAndGC

```go
func (rc *RedisCache) StartAndGC(config string) error
```
StartAndGC start redis cache adapter.

#### func (*RedisCache) connectInit

```go
func (rc *RedisCache) connectInit()
```
connectInit connect to redis server.
